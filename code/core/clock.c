#define DEFINE_CGU_REG	1
#include "clock.h"

#define XTAL_PD		(1<<0)
#define XTAL_BYPASS	(1<<1)
#define XTAL_HF		(1<<2)

/* set_xtal configures the operation of 
 * Since HF mode is don't care for clocks 15Mhz to 20Mhz, it is safe to always
 * set HF mode.
 * According to the datasheet, if BYPASS and PD are changed in one write the
 * clock can be unstable.
 */
void clk_set_xtal(bool enabled, bool bypass)
{
	uint32_t tmp = CGU->xtal_osc_ctrl | XTAL_HF;
	tmp &= ~XTAL_BYPASS;
	if (bypass)
		tmp |= XTAL_BYPASS;
	CGU->xtal_osc_ctrl = tmp;
	tmp &= ~XTAL_PD;
	if (!enabled)
		tmp |= XTAL_PD;
	CGU->xtal_osc_ctrl = tmp;
}

#define BASE_PD		(1<<0)
#define BASE_AUTOBLOCK	(1<<11)

/* configures the specified base clock with the given options: enabled,
 * autoblock clk_src
 */
void clk_set_base_clk(enum base_clk clk, bool en, bool autoblock, enum clk_src clk_src)
{
	uint32_t tmp = 0;
	if (!en)
		tmp |= BASE_PD;
	if (autoblock)
		tmp |= BASE_AUTOBLOCK;
	tmp |= clk_src << 24;
	CGU->base_clk[clk] = tmp;
}

/* waiting for 50us and 250us intervals is required to wait for clocks to
 * stablize
 * NOTE: assumes that it is run on the IRC clock (12MHz)
 */
void clk_irc_sleep50us(int n)
{
	for (int i = 0; i < n; i++) {
		__asm__ volatile (
					".rept 600	\n"
					"	nop	\n"
					".endr		\n"
				);
	}
}

#define IDIV_PD		(1<<0)
#define IDIV_AUTOBLOCK	(1<<11)
void clk_set_idiv(enum idiv_ctrl idev, bool en, bool autoblock, int div,
			enum clk_src clk_src)
{
	uint32_t tmp = 0;
	if (!en)
		tmp |= IDIV_PD;
	if (autoblock)
		tmp |= IDIV_AUTOBLOCK;
	tmp |= (div - 1) << 2;
	tmp |= clk_src << 24;
	CGU->idiv_ctrl[idev] = tmp;
}

#define PLL0_STATUS_MASK	(0x3)
#define PLL0_PD			(1<<0)
#define PLL0_BYPASS		(1<<1)
#define PLL0_DIRECTI		(1<<2)
#define PLL0_DIRECTO		(1<<3)
#define PLL0_CLKEN		(1<<4)
#define PLL0_FRM		(1<<6)
#define PLL0_AUTOBLOCK		(1<<11)

/*FROM THE DATASHEET -- wow, thats a lot of magic values...*/
static uint32_t mdec_new (uint32_t msel) {
	uint32_t x=0x4000, im;
	switch (msel) {
	case 0:
		return 0xFFFFFFFF;
	case 1:
		return 0x18003;
	case 2:
		return 0x10003;
	default:
		for (im = msel; im <= (1<<15); im++)
			x = ((x ^ x>>1) & 1) << 14 | (x>>1 & 0xFFFF);
		return x;
	}
}

static uint32_t anadeci_new (uint32_t msel) {
	uint32_t tmp;
	if (msel > 16384) return 1;
	if (msel > 8192) return 2;
	if (msel > 2048) return 4;
	if (msel >= 501) return 8;
	if (msel >= 60) {
		tmp=1024/(msel+9);
		return ( 1024 == ( tmp*(msel+9)) ) == 0 ? tmp*4 : (tmp+1)*4 ;
	}
	return (msel & 0x3c) + 4;
}

static uint32_t anadecp_new (uint32_t msel) {
	if (msel < 60)
		return (msel>>1) + 1;
	return 31;
}

static uint32_t ndec_new (uint32_t nsel) {
	uint32_t x=0x80, in;
	switch (nsel) {
	case 0:
		return 0xFFFFFFFF;
	case 1:
		return 0x302;
	case 2:
		return 0x202;
	default:
		for (in = nsel; in <= (1<<8); in++)
			x = ((x ^ x>>2 ^ x>>3 ^ x>>4) & 1) << 7 | (x>>1 & 0xFF);
		return x;
	}
}

static uint32_t pdec_new (uint32_t psel) {
	uint32_t x=0x10, ip;
	switch (psel) {
	case 0:
		return 0xFFFFFFFF;
	case 1:
		return 0x62;
	case 2:
		return 0x42;
	default:
		for (ip = psel; ip <= (1<<5); ip++)
			x = ((x ^ x>>2) & 1) << 4 | (x>>1 & 0x3F);
		return x;
	}
}

void clk_flags_pll0_usb(bool directi, bool directo, bool frm, bool autoblock,
				enum clk_src clksrc)
{
}

void clk_set_pll0_usb(bool en, int m, int n, int p)
{
	//SELP = COME FROM M
	//SELI = COME FROM M
	//SELR = 0
}

int clk_pll0_usb_status()
{
	return (CGU->pll0.usb_stat & PLL0_STATUS_MASK);
}


void clk_flags_pll0_audio(bool directi, bool directo, bool frm, bool autoblock,
				enum clk_src clksrc)
{
}

void clk_set_pll0_audio(bool en, int m, int n, int p)
{
}

int clk_pll0_audio_status()
{
	return (CGU->pll0.audio_stat & PLL0_STATUS_MASK);
}

#define PLL1_PD		(1<<0)
#define PLL1_BYPASS	(1<<1)
#define PLL1_FBSEL	(1<<6)
#define PLL1_DIRECT	(1<<7)
#define PLL1_AUTOBLOCK	(1<<11)
#define PLL1_LOCK	(1<<0)
#define PLL1_FLAGS_MASK	(PLL1_BYPASS | PLL1_FBSEL | PLL1_AUTOBLOCK | (0x1F << 24))
#define PLL1_SET_MASK	(PLL1_PD | PLL1_DIRECT | (0x3 << 8) | (0x3 << 12) | (0xFF << 16))

void clk_flags_pll1(bool bypass, bool fbsel, bool autoblock, enum clk_src clksrc)
{
	uint32_t tmp = CGU->pll1.ctrl & ~PLL1_FLAGS_MASK;
	if (bypass)
		tmp |= PLL1_BYPASS;
	if (fbsel)
		tmp |= PLL1_FBSEL;
	if (autoblock)
		tmp |= PLL1_AUTOBLOCK;
	tmp |= clksrc << 24;
	CGU->pll1.ctrl = tmp;
}

void clk_set_pll1(bool en, bool direct, int psel, int nsel, int msel)
{
	uint32_t tmp = CGU->pll1.ctrl & ~PLL1_SET_MASK;
	if (!en)
		tmp |= PLL1_PD;
	if (direct)
		tmp |= PLL1_DIRECT;
	tmp |= psel << 8;
	tmp |= (nsel - 1) << 12;
	tmp |= (msel - 1) << 16;
	CGU->pll1.ctrl = tmp;
}

bool clk_pll1_locked()
{
	return !!(CGU->pll1.stat & PLL1_LOCK);
}
