#ifndef CLOCK_H
#define CLOCK_H

#include "def.h"

enum base_clk
{
	BASE_SAFE_CLK,
	BASE_USB0_CLK,
	BASE_PERIPH_CLK,
	BASE_USB1_CLK,
	BASE_M4_CLK,
	BASE_SPIFI_CLK,
	BASE_SPI_CLK,
	BASE_PHY_RX_CLK,
	BASE_PHY_TX_CLK,
	BASE_APB1_CLK,
	BASE_APB3_CLK,
	BASE_LCD_CLK,
	BASE_ADCHS_CLK,
	BASE_SDIO_CLK,
	BASE_SSP0_CLK,
	BASE_SSP1_CLK,
	BASE_UART0_CLK,
	BASE_UART1_CLK,
	BASE_UART2_CLK,
	BASE_UART3_CLK,
	BASE_OUT_CLK,
	/*RESERVED 1,2,3,4*/
	BASE_AUDIO_CLK = BASE_OUT_CLK + 5,
	BASE_CGU_OUT0_CLK,
	BASE_CGU_OUT1_CLK,
	BASE_CLK_COUNT,
};

enum clk_src
{
	SRC_CLK_RTC,
	SRC_CLK_IRC,
	SRC_CLK_ENET_RX,
	SRC_CLK_ENET_TX,
	SRC_CLK_GP_CLK,
	/*RESERVED*/
	SRC_CLK_XTAL = SRC_CLK_GP_CLK + 2,
	SRC_CLK_PLL0USB,
	SRC_CLK_PLL0AUDIO,
	SRC_CLK_PLL1,
	SRC_CLK_IDIVA,
	SRC_CLK_IDIVB,
	SRC_CLK_IDIVC,
	SRC_CLK_IDIVD,
	SRC_CLK_IDIVE,
};

enum idiv_ctrl
{
	IDIVA,
	IDIVB,
	IDIVC,
	IDIVD,
	IDIVE,
	IDIV_COUNT,
};

#ifndef DEFINE_CGU_REG
#define DEFINE_CGU_REG 0
#endif

#if DEFINE_CGU_REG
struct CGU_reg
{
	/*RESERVED: please don't touch me.*/
	RO uint32_t r0[5];
	
	RW uint32_t freq_mon;
	
	RW uint32_t xtal_osc_ctrl;
	
	struct {
		RO uint32_t usb_stat;
		RW uint32_t usb_ctrl;
		RW uint32_t usb_mdiv;
		RW uint32_t usb_np_div;
	
		RO uint32_t audio_stat;
		RW uint32_t audio_ctrl;
		RW uint32_t audio_mdiv;
		RW uint32_t audio_np_div;
		RW uint32_t audio_frac;
	} PACKED pll0;
	
	struct {
		RO uint32_t stat;
		RW uint32_t ctrl;
	} PACKED pll1;
	
	RW uint32_t idiv_ctrl[IDIV_COUNT];
	RW uint32_t base_clk[BASE_CLK_COUNT];
} PACKED;

struct CGU_reg * const CGU = (void*)0x40050000;

#endif

void clk_set_xtal(bool enabled, bool bypass);

void clk_set_base_clk(enum base_clk clk, bool en, bool autoblock,
			enum clk_src clk_src);

/* IDIVA Range: 1-4
 * IDIVB/C/D Range: 1-16
 * IDIVE Range: 1-256
 */
void clk_set_idiv(enum idiv_ctrl idev, bool en, bool autoblock, int div,
			enum clk_src clk_src);

/*normal flags: directi = false, directo = false, frm = false, autoblock = false
 * PLL0 status bit 0 - locked, bit 1 - free running
 * m: 1-2**15
 * n: 1-2**8
 * p: 1-2**5
 */
void clk_flags_pll0_usb(bool directi, bool directo, bool frm, bool autoblock,
				enum clk_src clksrc);
void clk_set_pll0_usb(bool en, int m, int n, int p);
int clk_pll0_usb_status();

void clk_flags_pll0_audio(bool directi, bool directo, bool frm, bool autoblock,
				enum clk_src clksrc);
void clk_set_pll0_audio(bool en, int m, int n, int p);
int clk_pll0_audio_status();

/*normal flags: bypass = false, fb = false, autoblock = false, CLK_SRC_XTAL
 * psel: 0-3 maps to 2**psel
 * nsel: 1-4
 * msel: 1-256
 * direct disables division by p
 * output frequency = m/(p*n) * in
 * 
 * frequency given by (m / n * in) MUST be in the range 156MHz - 320MHz
 * 
 * input frequency must be in the range 10Mhz - 25Mhz
 */
void clk_flags_pll1(bool bypass, bool fbsel, bool autoblock, enum clk_src clksrc);
void clk_set_pll1(bool en, bool direct, int psel, int nsel, int msel);
bool clk_pll1_locked();

void clk_irc_sleep50us(int n);

#endif

