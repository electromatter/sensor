#ifndef DEF_H
#define DEF_H

#include <stdint.h>
#include <stdbool.h>

#define PACKED __attribute__((packed))

#define RO	const volatile
#define WO	volatile
#define RW	volatile

#endif
