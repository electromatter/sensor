/*
FROM ARM DOCUMENTS:
Excep	irq	vector
16+n	 n	IRQn
		...
18	 2	IRQ2
17	 1	IRQ1
16	 0	IRQ0
15	-1	Systick
14	-2	PendSV
13		Reserved
12		Reserved for Debug
11	-5	SVCall
10		RESERVED
 9		RESERVED
 8		RESERVED
 7		RESERVED
 6	-10	Usage Fault
 5	-11	Bus Fault
 4	-12	Memory Management Fault
 3	-13	Hard Fault
 2	-14	NMI
 1		Reset
 0		Inital SP

FROM NXP DOCS:
irq	function	flags
 0	DAC		-
 1	MAPP		Cortex-M0APP; Latched TXEV; for M4-M0APP communication
 2	DMA		-
 3	RESERVED	RESERVED
 4	FLASHEEPROM	ORed flash bank A, flash bank B, EEPROM interrupts
 5	ETHERNET	Ethernet interrupt
 6	SDIO		SD/MMC interrupt
 7	LCD		-
 8	USB0		OTG interrupt
 9	USB1		-
10	SCT		SCT combined interrupt
11	RITIMER		-
12	TIMER0		-
13	TIMER1		-
14	TIMER2		-
15	TIMER3		-
16	MCPWM		Motor control PWM
17	ADC0		-
18	I2C0		-
19	I2C1		-
20	SPI		-
21	ADC1		-
22	SSP0		-
23	SSP1		-
24	USART0		-
25	UART1		Combined UART interrupt with Modem interrupt
26	USART2		-
27	USART3		Combined UART interrupt with Modem interrupt
28	I2S0		-
29	I2S1		-
30	SPIFI		-
31	SGPIO		-
32	PIN_INT0	GPIO pin interrupt 0
33	PIN_INT1	GPIO pin interrupt 1
34	PIN_INT2	GPIO pin interrupt 2
35	PIN_INT3	GPIO pin interrupt 3
36	PIN_INT4	GPIO pin interrupt 4
37	PIN_INT5	GPIO pin interrupt 5
38	PIN_INT6	GPIO pin interrupt 6
39	PIN_INT7	GPIO pin interrupt 7
40	GINT0		GPIO global interrupt 0
41	GINT1		GPIO global interrupt 1
42	EVENTROUTER	Event router interrupt
43	C_CAN1		-
44	RESERVED	-
45	ADCHS		ADCHS combined interrupt
46	ATIMER		Alarm timer interrupt
47	RTC		-
48	RESERVED	-
49	WWDT		-
50	M0SUB		TXEV instruction from the M0 subsystem core
51	C_CAN0		-
52	QEI		-
*/

.section .stack
.globl stacktop
stackbottom:
.skip 8192
stacktop:

.section .text
.globl _start
hang:
	b hang

.section .intvec
.global intvec
intvec:
.word stacktop    /* stack top address */
.word _start + 1      /* 1 Reset */
.word hang + 1        /* 2 NMI */
.word hang + 1        /* 3 HardFault */
.word hang + 1        /* 4 MemManage */
.word hang + 1        /* 5 BusFault */
.word hang + 1        /* 6 UsageFault */
.word -((stacktop) + (_start + 1) + (hang + 1) + (hang + 1) + (hang + 1) + (hang + 1) + (hang + 1))        /* 7 RESERVED - SHOULD BE CHECKSUM: first 8 fields should add to zero*/
.word hang + 1        /* 8 RESERVED */
.word hang + 1        /* 9 RESERVED*/
.word hang + 1        /* 10 RESERVED */
.word hang + 1        /* 11 SVCall */
.word hang + 1        /* 12 Debug Monitor */
.word hang + 1        /* 13 RESERVED */
.word hang + 1        /* 14 PendSV */
.word hang + 1        /* 15 SysTick */
/* IRQ VECTORS */
.rept 53
.word hang + 1
.endr

/* for M4 -- 16+53 = 69 intvec enteries
 * for M0 -- 16+32 = 48 intvec enteries
 */
