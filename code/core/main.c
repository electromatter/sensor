#include "clock.h"

static void setup_clocks()
{
	clk_set_base_clk(BASE_M4_CLK, true, true, SRC_CLK_IRC);

	clk_set_xtal(true, false);
	clk_irc_sleep50us(5);

	clk_flags_pll1(false, false, false, SRC_CLK_XTAL);
	clk_set_pll1(true, false, 1, 2, 16);
	
	while (!clk_pll1_locked());

	clk_set_base_clk(BASE_M4_CLK, true, true, SRC_CLK_PLL1);
	clk_irc_sleep50us(1);

	clk_set_pll1(true, true, 1, 2, 16);
}

// should reduce power usage
static void halt()
{
	__asm__ volatile ("wfi");
}

void start()
{
	//START:
	//PLL
	//RTC
	//UART
	//USART
	//CAN
	//ETHERNET
	//USB
	//I2C
	//SPI
	//IRQ

	//SETUP CLOCK
	setup_clocks();
	
	//init rtc
	//init uart
	//init ethernet
	
	//checksum rom
	
	//enable M0

	//checksum rom
	
	//initalize data section
	
	//init drivers
	//init userspace
	
	//mainloop
	
	while (1) {
		halt();
	}
}

