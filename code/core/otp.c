#include <stdint.h>

uint32_t chip_otp_init(void)
{
	uint32_t **api_table = (void*)0x10400100;
	uint32_t *otp_api_table = api_table[1];
	typedef uint32_t (*initfp_t)(void);
	initfp_t init = (initfp_t)otp_api_table[0];
	return init();
}

typedef enum CHIP_OTP_BOOT_SRC {
	PINS,
	UART0,
	SPIFI,
	EMC8,
	EMC16,
	EMC32,
	USB0,
	USB1,
	SPI,
	UART3,
} CHIP_OTP_BOOT_SRC_T;

uint32_t chip_otp_progbootsrc(CHIP_OTP_BOOT_SRC_T src)
{
	uint32_t **api_table = (void*)0x10400100;
	uint32_t *otp_api_table = api_table[1];
	typedef uint32_t (*bootfp_t)(CHIP_OTP_BOOT_SRC_T);
	bootfp_t init = (bootfp_t)otp_api_table[1];
	return init(src);
}

uint32_t test()
{
	return 100;
}

void start(void)
{
	while (1) {}
}

