#ifndef UART_H
#define UART_H

#define USART3_BASE		(0x400C2000)

#define USART_RBR		(0x00)
#define USART_THR		(0x00)
#define USART_DLL		(0x00)
#define USART_DLM		(0x04)
#define USART_IER		(0x04)
#define USART_IRR		(0x08)
#define USART_FCR		(0x08)
#define USART_LCR		(0x0C)
#define USART_LSR		(0x14)
#define USART_SCR		(0x1C)
#define USART_ICR		(0x24)
#define USART_FDR		(0x28)
#define USART_OSR		(0x2C)
#define USART_HDEN		(0x40)
#define USART_SYNCCTRL		(0x58)
#define USART_TER		(0x5C)

struct UART
{
};

#endif
