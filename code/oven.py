#!env python
 
import sys
import base64
import serial
 
def _devarint(data):
	shift = 0
	ret = 0
	for c in data:
		byte = ord(c)
		ret |= (byte & 0x7F) << shift
		shift += 7
		if (byte & 0x80) == 0:
			return ret
	return -1
 
def _volt(val):
	return val * (5.0 / 1023.0)
 
def _temp(volt):
	return (volt - 1.25) / 0.005
 
class oven:
 
	def __init__(self, path):
		self.ser = serial.Serial(path, 115200, timeout = 1)
	def poll(self, powered = False):
		if powered:
			self.ser.write("1")
		else:
			self.ser.write("0")
		line = self.ser.readline()
		data = base64.b64decode(line)
		val = _devarint(data)
		v = _volt(val)
		c = _temp(v)
		return c
 
from collections import deque
import time
 
import numpy
 
class window:
	def __init__(self, width):
		self.clear()
		self.width = width
	def __iter__(self):
		return self.np.__iter__()
	def __len__(self):
		return self.np.size
	def __getitem__(self, x):
		return self.np[x]
	def clear(self):
		self.np = numpy.empty(0)
	def append(self, x):
		if len(self) == self.width:
			self.np = numpy.roll(self.np, -1)
			self.np[-1] = x
		else:
			self.np = numpy.append(self.np, x)
	def stats(self):
		sum = 0
		sum2 = 0
		n = len(self)
		if n == 0:
			return (0, 0)
		for x in self:
			sum += x
			sum2 += x * x
		mu = sum / n
		var = (sum2 / n) - (mu ** 2)
		return (mu, var)
 
import threading
 
running = False
 
def do_oven(o, ws, dws, tws, dt):
	while True:
		time.sleep(dt)
		temp = o.poll(running)
		try:
			last = ws.itervalues().next()[-1]
		except IndexError:
			last = temp
		dx = (temp - last) / dt
		for T, dw in dws.iteritems():
			dw.append(dx)
		for T, w in ws.iteritems():
			w.append(temp)	
		for T, tw in tws.iteritems():
			tw.append(time.time())
 
def windows(dt, Ts):
	ws = {}
	for T in Ts:
		slices = T / dt
		ws[T] = (window(slices))
	return ws
 
import wx
from wxmplot import plotpanel
 
class ControlFrame(wx.Frame):
	def __init__(self, ws, dws, tws):
		wx.Frame.__init__(self, None, wx.ID_ANY, "Oven Control",
			size = (500,500))
		self.timer = wx.Timer(self)
		self.Bind(wx.EVT_TIMER, self.update, self.timer)
		self.timer.Start(500)
		self.plot = plotpanel.PlotPanel(self)
		self.ws = ws
		self.dws = dws
		self.tws = tws
		self.w = window(50)
		self.t = window(50)
 
		self.bottom = wx.Panel(self)
		self.start = wx.Button(self.bottom, label = "Start")
		self.stop = wx.Button(self.bottom, label = "Stop")
		self.running = wx.TextCtrl(self.bottom, value = "Not Running", style = wx.TE_READONLY)
		self.dy = wx.TextCtrl(self.bottom, value = "dy=", style = wx.TE_READONLY)
		self.temp = wx.TextCtrl(self.bottom, value = "y=", style = wx.TE_READONLY)
 
		sz = wx.BoxSizer()
		sz.Add(self.start, 1, wx.EXPAND | wx.ALL, 5)
		sz.Add(self.stop, 1, wx.EXPAND | wx.ALL, 5)
		sz.Add(self.running, 1, wx.EXPAND | wx.ALL, 5)
		sz.Add(self.dy, 1, wx.EXPAND | wx.ALL, 5)
		sz.Add(self.temp, 1, wx.EXPAND | wx.ALL, 5)
		self.bottom.SetSizer(sz)
 
		self.start.Bind(wx.EVT_BUTTON, self.startcb)
		self.stop.Bind(wx.EVT_BUTTON, self.stopcb)
 
		sz = wx.BoxSizer(orient = wx.VERTICAL)
		sz.Add(self.plot, 10, wx.EXPAND)
		sz.Add(self.bottom, 1, wx.EXPAND)
		self.SetSizer(sz)
	def update(self, event):
		temp = self.ws[1].stats()[0]
		dy = self.dws[1].stats()[0]
		self.w.append(temp)
		self.t.append(time.time())
		self.plot.plot(self.t.np, self.w.np, ymin = 0, ymax = 300)
		if running:
			self.running.SetValue("Running")
			self.running.SetBackgroundColour(wx.GREEN)
		else:
			self.running.SetValue("Not Running")
			self.running.SetBackgroundColour(wx.RED)
		self.dy.SetValue("dy=%f" % dy)
		self.temp.SetValue("y=%f" % temp)
	def startcb(self, event):
		global running
		running = True
	def stopcb(self, event):
		global running
		running = False
#TODO setpoints
#now for threading!
def main(argv):
	dt = 1 / 200.
 
	ws = windows(dt, (1, 2, 5, 10))
	dws = windows(dt, (1, 2, 5, 10))
	tws = windows(dt, (1, 2, 5, 10))
 
	o = oven(argv[1])
 
	t = threading.Thread(target = do_oven, args = (o, ws, dws, tws, dt))
	t.daemon = True
	t.start()
 
	app = wx.App()
	control = ControlFrame(ws, dws, tws)
	control.Show(True)
		
	app.MainLoop()
 
import sys
 
if __name__ == '__main__':
	main(sys.argv)
